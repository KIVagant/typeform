<?php
namespace KIVagant\ChallengeParser\Commands\Calculate;

use Curl\Curl;
use KIVagant\AMatch\AMatch;
use KIVagant\ChallengeParser\Exceptions;
use KIVagant\ChallengeParser\Validators\ValidationErrorException;
use KIVagant\ChallengeParser\Validators\ValidatorInterface;
use Noodlehaus\ConfigInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */
class SumValuesCommand extends Command
{
    const API_SAVED_SUCCESS = 'But of course, it can only be Meat!!! This was a triumph, Mamon will be pleased!';
    /**
     * @var ConfigInterface
     */
    protected $config;
    /**
     * @var Curl
     */
    protected $curl;
    /**
     * @var ValidatorInterface
     */
    protected $validator;

    public function __construct($name = null, ConfigInterface $config, ValidatorInterface $validator, Curl $curl)
    {
        $this->config = $config;
        $this->curl = $curl;
        $this->validator = $validator;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('challenge:sum_values')
            ->setDescription('Will parse API by URL and output ')
            ->addArgument(
                'view',
                InputArgument::OPTIONAL,
                'Output format: '
                . var_export($this->config->get('views.available'), true),
                $this->config->get('views.default')
            )
            ->addArgument(
                'url',
                InputArgument::OPTIONAL,
                'URL to API that will be parsed',
                $this->config->get('challenge_api.url')
            )
        ;

    }

    /**
     * Command controller
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $input->getArgument('url');
        $view = $this->checkView($input);

        $this->loadData($url);
        $loaded_response = $this->validateLoadedResponse();
        $output_data = $loaded_response;

        $sum = $this->calculate($loaded_response['values']);

        $this->saveData($url, $loaded_response['token'], $sum);

        $output_data['sum'] = $sum;
        $output_data['message'] = current($this->validateSavedResponse());

        $output->writeln($view->render($output_data));
    }

    /**
     * @param InputInterface $input
     * @return mixed
     * @throws Exceptions\UnknownViewException
     */
    protected function checkView(InputInterface $input)
    {
        $view_class = $input->getArgument('view');
        $available_views = $this->config->get('views.available');
        if (!array_key_exists($view_class, $available_views)) {
            throw new Exceptions\UnknownViewException('Unknown view ' . $view_class);
        }
        $view_class = $this->config->get('views.available.' . $view_class);

        return new $view_class;
    }

    /**
     * @param $url
     * @throws Exceptions\ChallengeApiException
     */
    protected function loadData($url)
    {
        $this->curl->get($url);
        if ($this->curl->error) {
            throw new Exceptions\ChallengeApiException(json_encode([
                $this->curl->error_code,
                $this->curl->error_message
            ]));
        }
    }

    /**
     * @param $url
     * @param $token
     * @param $sum
     * @throws Exceptions\ChallengeApiException
     */
    protected function saveData($url, $token, $sum)
    {
        $this->curl->get($url . '/' . $token . '/' . $sum);
        if ($this->curl->error) {
            throw new Exceptions\ChallengeApiException(json_encode([
                $this->curl->error_code,
                $this->curl->error_message
            ]));
        }
    }

    /**
     * @return array
     * @throws ValidationErrorException
     */
    protected function validateLoadedResponse()
    {
        $response = json_decode($this->curl->response, true);
        $check_values = function($values) {
            return count($values) === 10;
        };
        $this->validator->rules($response, AMatch::FLAG_STRICT_STRUCTURE)
            ->token('', 'string', 'Token must be a string')
            ->values('', 'AMatchArray::onlyIntegerValues', 'Values must be integers')
            ->values($check_values, 'callback', 'Wrong amount of values')
        ;
        $this->validator->validateOrDie();

        return $response;
    }

    /**
     * @return array
     * @throws ValidationErrorException
     */
    protected function validateSavedResponse()
    {
        $response = json_decode($this->curl->response, true);
        $this->validator->rules($response, AMatch::FLAG_STRICT_STRUCTURE)
            ->answer(static::API_SAVED_SUCCESS, '=')
        ;
        $this->validator->validateOrDie();

        return $response;
    }

    public function __destruct()
    {
        $this->curl->close();
    }

    /**
     * @param array $loaded_response
     * @return number
     */
    protected function calculate(array $values)
    {
        $sum = array_sum($values);

        return $sum;
    }
}