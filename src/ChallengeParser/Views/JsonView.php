<?php
namespace KIVagant\ChallengeParser\Views;

class JsonView implements ViewInterface
{
    public function render(array $data)
    {
        return json_encode($data);
    }
}