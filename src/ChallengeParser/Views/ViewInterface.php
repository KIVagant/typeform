<?php
namespace KIVagant\ChallengeParser\Views;

interface ViewInterface
{
    public function render(array $data);
}