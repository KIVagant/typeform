<?php
namespace KIVagant\ChallengeParser\Views;

class PlainTextView implements ViewInterface
{
    public function render(array $data)
    {
        $result = 'Result:' . PHP_EOL;
        foreach ($data as $k => $v) {
            $result .= $k . ': ' . var_export($v, true) . PHP_EOL;
        }

        return $result;
    }
}