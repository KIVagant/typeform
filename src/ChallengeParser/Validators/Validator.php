<?php
namespace KIVagant\ChallengeParser\Validators;
use KIVagant\AMatch\AMatch;

class Validator implements ValidatorInterface
{
    /**
     * @var AMatch
     */
    protected $match;
    /**
     * @param array $array
     * @return AMatch
     */
    public function rules(&$actual_ar, $flags = AMatch::NO_FLAGS, $statuses_mapping_object = null)
    {
        $this->match = new AMatch($actual_ar, $flags, $statuses_mapping_object);

        return $this->match;
    }

    public function validate()
    {
        return $this->match->stopMatch();
    }

    public function getErrors()
    {
        return $this->match->matchComments();
    }
    public function getErrorsDescriptions()
    {
        return $this->match->matchCommentsConditions();
    }

    /**
     * @throws ValidationErrorException
     */
    public function validateOrDie()
    {
        if(!$this->validate()) {
            throw new ValidationErrorException('Validation errors:' . $this);
        }
    }
    public function __toString()
    {
        return '' . print_r($this->getErrors(), true);
    }
}