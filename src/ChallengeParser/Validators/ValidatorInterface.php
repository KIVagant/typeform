<?php
namespace KIVagant\ChallengeParser\Validators;

use KIVagant\AMatch\AMatch;

interface ValidatorInterface
{
    public function rules(&$actual_ar, $flags = AMatch::NO_FLAGS, $statuses_mapping_object = null);
    public function validate();
    public function __toString();
    public function validateOrDie();
}