<?php
/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */

return [
    'services' => [
        'browser' => [
            'class' => \Curl\Curl::class,
            'arguments' => [],
        ],
        'validator' => [
            'class' => \KIVagant\ChallengeParser\Validators\Validator::class,
            'arguments' => [],
        ],
        'challenge:sum_values' => [
            'class' => \KIVagant\ChallengeParser\Commands\Calculate\SumValuesCommand::class,
            'arguments' => [
                '',
                '@config',
                '@validator',
                '@browser',
            ],
        ],
    ],
];