<?php

return [
    'views' => [
        'available' => [
            'plain_text' => KIVagant\ChallengeParser\Views\PlainTextView::class,
            'json' => KIVagant\ChallengeParser\Views\JsonView::class,
        ],
        'default' => 'plain_text',
    ],
];