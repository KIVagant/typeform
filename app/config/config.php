<?php

return [
    // Command that will be executed by default
    'default_command' => 'challenge:sum_values',

    // Each command should be registered as service, see services.php
    'commands' => [
        'challenge:sum_values',
    ],

    'challenge_api' => [
        'url' => 'http://aerial-valor-93012.appspot.com/challenge',
    ],
];