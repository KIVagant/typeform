# Challenge API parser (Typeform test)

A PHP commandline tool that implements this specification: [http://aerial-valor-93012.appspot.com/]

## Usage:
``` php index.php [commandname] [format] [api_url]```

## Examples:

* ``` php index.php```
* ``` php index.php challenge:sum_values plain_text```
* ``` php index.php challenge:sum_values plain_text http://google.com```
* ``` php index.php challenge:sum_values plain_text http://aerial-valor-93012.appspot.com/challenge```
* ``` php index.php challenge:sum_values json```

## Notes:

* Not-Symfony config package was used because Symfony components sometimes too ponderous.
* Validator based on my old library. For usage in production environment, I will need to upgrade this library to make it more flexible and clear.