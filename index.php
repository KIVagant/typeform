<?php
require __DIR__ . '/vendor/autoload.php';

$application = new \KIVagant\ChallengeParser\ChallengeParser();
$application->registerServices();
$application->registerCommands();
$application->run();
